/**
* Account schema
* Copyright(c) 2015 Mithun Das (https://github.com/mithundas79)
* MIT Licensed
*/

var mongoose = require('mongoose'),
    Bcrypt   = require('bcrypt-nodejs'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var AccountSchema = new Schema({
    email				: {
    	type: String,
    	required: true,
    	trim: true,
    	index: {
    		unique: true,
    		sparse: true
    	}
    },
    password			: {
    	type: String, 
    	required: true
    },
    status	    : {
    	type: Boolean,
        default: true
    },
    type	    : {     /// cam be A or B only
    	type: String,
        default: "A"
    },
    modified		: {
        type: Date,
        default: Date.now
    }
});

/**
Before save argument for the user schema...
Hashes the password before saving
*/
AccountSchema.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  Bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    Bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});

module.exports = mongoose.model('Account', AccountSchema);