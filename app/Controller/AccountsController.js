/**
Account controller
*/

var Account = require('../Model/Account');

exports.welcome = function(req, res) {
    res.json({ message: 'Welcome!' });
}

// Create endpoint /api/accounts for POST
exports.postAccounts = function(req, res) {
    var params = req.body;
    var newAccount = {};

    for (prop in params) {
        newAccount[prop] = params[prop];
    }
    console.log(newAccount);
    var account = new Account(newAccount);

    account.save(function(err) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json({ message: 'Account added to repositary!' });
        }

    });
};

// Create endpoint /api/accounts/batch for POST
exports.postBulkAccounts = function(req, res) {
    var params = req.body;
    var newAccounts = [];
        
    var jsonObj = jsonToggler(params);
    console.log(jsonObj.length);
    Account.collection.insert(jsonObj, function (err, accounts) {
        if(err) {
            res.json({ status: 0, message: err});
        } else {
            res.json(jsonObj.length + " inserts initialized");
        }
    });
    
    
};

// Create endpoint /api/accounts for GET
exports.getAccounts = function(req, res) {
    Account.find(function(err, accounts) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(accounts);
        }    
    });
};

// Create endpoint /api/account/:id for GET
exports.getAccount = function(req, res) {
    var accountId = req.params.id;
    Account.findOne({ _id: accountId }, function(err, account) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            res.json(account);
        }
    });
    
};

// Create endpoint /api/account/:id for PUT
exports.updateAccount = function(req, res) {
    var accountId = req.params.id;
    var param = req.body;
    Account.findOne({ _id: accountId }, function(err, account) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update account
            for (prop in param) {
                account[prop] = param[prop];
            }
            //save the account
            account.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    res.json({ message: 'Account is updated!' });
                }
            });

        }
    });
}
// Create endpoint /api/account/:id for DELETE
exports.deleteAccount = function(req, res) {
    var accountId = req.params.id;
    Account.remove({ _id: accountId }, function(err) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            res.json({ message: 'Account is deleted!' });
        }
    });
}

// Create endpoint /api/account/:id/activate for GET
exports.accountActivate = function(req, res) {
    var accountId = req.params.id;
    var param = { status: true };
    Account.findOne({ _id: accountId }, function(err, account) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update account
            for (prop in param) {
                account[prop] = param[prop];
            }
            //save the account
            account.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    res.json({ message: 'Account is activated!' });
                }
            });

        }
    });
}

// Create endpoint /api/account/:id/deactivate for GET
exports.accountDeactivate = function(req, res) {
    var accountId = req.params.id;
    var param = { status: false };
    Account.findOne({ _id: accountId }, function(err, account) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update account
            for (prop in param) {
                account[prop] = param[prop];
            }
            //save the account
            account.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    res.json({ message: 'Account is deactivated!' });
                }
            });

        }
    });
}

/**

Toggles from json to json arrays if json is passed
Toggles from json of arrays into an array of jsons if json of arrays is passed

@Usage
var jsonOfArrays =  { username: [ 'roy', 'nick', 'neil' ],
  password: [ '1234', '12345', '123456' ] };

JSON.stringify(jsonToggler(params)) will return

[ {username: 'roy', password: '1234'},  {username: 'nick', password: '12345'},  {username: 'neil', password: '123456'}]

and vice versa

@param mixed my_object 
@version 1.0
@copyright Mithun Das <github.com/mithundas79>
**/
function jsonToggler(my_object) {
    if (my_object instanceof Array) {
        //convert array of json to json of arrays
        var a_keys = new Array();
        var my_output = {};

        if (my_object.length > 0) {
            for (my_key in my_object[0]) {
                a_keys.push(my_key);
            }
        }
        for (var j = 0; j < a_keys.length; j++) {
            var a_cell = new Array();
            for (var i = 0; i < my_object.length; i++) {
                if (my_object[i][a_keys[j]] != undefined) {
                    a_cell.push(my_object[i][a_keys[j]]);
                } else {
                    a_cell.push("");
                }
            }
            my_output[a_keys[j]] = a_cell;
        }
        return my_output;
    } else {
        //convert json of arrays into an array of json
        var my_output = new Array();
        var a_keys = new Array();
        for (my_key in my_object) {
            a_keys.push(my_key);
        }
        if (a_keys.length > 0) {
            for (var i = 0; i < my_object[a_keys[0]].length; i++) {
                var my_json = {};
                for (var j = 0; j < a_keys.length; j++) {
                    my_json[a_keys[j]] = my_object[a_keys[j]][i];
                }
                my_output.push(my_json);
            }
        }
        return my_output;
    }
}